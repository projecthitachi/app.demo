﻿var tracing_zone = [];

var realtime_tracing_timeline = [];
var setuuid = "";
var setlocID = "";
var setdate = "";
var dateString = "";
var uuid = "";

var dataLengthRealtime = 0;
var date1Realtime;
var date2Realtime;
var diffRealtime = 0;

function contactTracing(uuid = "", dateString = "") {
    var val = $("#contact-tracing-userId").val();
    if ((uuid == "" || uuid == undefined) && val != undefined && val != "") {
        uuid = val;
    }
    if (uuid == "" || uuid == undefined) {
        uuid = null;
    }

    //start load new content realtime
    if ($("#dateContactTracing").data("kendoCalendar") != undefined) {
        var dateContactTracing = $("#dateContactTracing").data("kendoCalendar").value();
        var dateString = kendo.toString(dateContactTracing, "yyyy-MM-dd HH:mm:ss");
        var dateHeader = kendo.toString(dateContactTracing, "dd-MM-yyyy");
    }
    if (dateString == null) {
        kendo.alert("Date is empty.");
    }
    if (uuid == null) {
        kendo.alert("Person ID is empty.");
    }

    setuuid = val;
    setdate = dateString;
    //fungai dPET list time
    $.ajax({
        type: 'POST',
        url: base_url + 'Widget/SmartwatchDataRoomDetail?uuid=' + uuid + '&&startdate=' + dateString,
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function () {
        },
        success: function (res) {
            // check data null
            if (res['data'].length < 1) {
                kendo.alert("Data With uuid " + uuid + " not found.");
            } else {
                tracing_zone = [];
                // Set Collor Visited Zone 
                for (var i = 0; i < res['VisitedZone'].length; i++) {
                    tracing_zone.push(res['VisitedZone'][i]._id.toString());
                }

                removeBox("alert");
                clearPlayback();
                $(".button-icon-contact-tracing").addClass("active");
                $("#devicebox-button-contact-tracing img").attr("src", base_url + "Content/assets/images/contact-tracing-active.png");
                $("#box-alert").html("");
                $("#box-sensor").html("");
                $("#box-human-presence").html("");
                $("#windowPopup").data("kendoWindow").close();
                filter_tracing_active = true;

                human_building();

                var _itemTemplate = kendo.template($("#tmpl-WidgetRealtimeContactTracing").html(), { useWithBlock: false });
                jres = res['data'];
                resdata = [];
                for (var i = 0; i < jres.length; i++) {
                    var milli = jres[i].RecordTimestamp.replace(/\/Date\((-?\d+)\)\//, '$1');
                    var date = new Date(parseInt(milli));
                    var datetext = kendo.toString(date, "HH:mm");
                    var datetext2 = kendo.toString(date, "yyyy-MM-dd HH:mm:ss");
                    var j = {
                        RecordTimestamp: datetext,
                        Time: datetext2,
                        location: jres[i].location,
                        locationID: jres[i].locationID,
                        personID: jres[i].personID,
                        personName: jres[i].personName,
                        status: jres[i].status,
                        top: jres[i].top,
                        left: jres[i].left,
                        _id: jres[i]._id,
                        inRoom: jres[i].inRoom,
                        floor: jres[i].location.slice(0, 3).slice(1),
                    };
                    resdata.push(j);
                    realtime_tracing_timeline.push(j);

                    if (setuuid == "") {
                        setuuid = jres[0].personID;
                    }
                    if (setlocID == "") {
                        setlocID = jres[0].locationID;
                    }
                }
                var TV = res['VisitedZone'].length;
                let data_realtime = {
                    TotalVisited: TV,
                    TotalContacted: res['TotalContacted'],
                    uuid: res['uuid'],
                    personName: res['personName'],
                    data: resdata,
                    colorZone: res['allZone'],
                    date: kendo.toString(resdata[0].Time, "yyyy-MM-dd HH:mm:ss")
                };
                var result = _itemTemplate(data_realtime); //Pass the data to the compiled template
                $("#wrapper-widget-realtime .widget-content").html(result);
                $("#wrapper-widget-realtime .widget-footer").addClass("hide");
                var today = kendo.toString(new Date(), "dd-MM-yyyy");
                $(".widget-realtime-content .title-realtime").html("Contact Tracing - " + dateHeader);
                $(".realtime-ContactTracing").parent().css("max-height", "525px");
                $(".realtime-ContactTracing").closest(".widget-realtime-content").css("width", "420px");

                if ($("#dateContactTracing").data("kendoDatePicker") != undefined) {
                    var dateContactTracing = $("#dateContactTracing").data("kendoDatePicker").value();
                    var dateString = kendo.toString(dateContactTracing, "dd-MM-yyyy HH:mm:ss");
                    $("#headingRealtime span.title-realtime").html("Contact Tracing - " + dateHeader);
                    //console.log(dateString);
                }

                //end load new content realtime

                //show playback
                var _itemPlaybackTracing = kendo.template($("#tmpl-WidgetFloatingPlaybackContactTracing").html(), { useWithBlock: false });
                var resultPlaybackTracing = _itemPlaybackTracing({});
                $("#box-playback-tracing").html(resultPlaybackTracing);

                var dataLength = res['data'].length;

                var milli2 = res['data'][0].RecordTimestamp.replace(/\/Date\((-?\d+)\)\//, '$1');
                var date2 = new Date(parseInt(milli2));
                var datetext2 = kendo.toString(date2, "yyyy-MM-dd HH:mm:ss");

                var milli3 = res['data'][dataLength - 1].RecordTimestamp.replace(/\/Date\((-?\d+)\)\//, '$1');

                var date3 = new Date(parseInt(milli3));
                var datetext3 = kendo.toString(date3, "yyyy-MM-dd HH:mm:ss");

                var date1 = new Date(datetext2);
                var date2 = new Date(datetext3);
                var diff = (date2.getTime() - date1.getTime()) / 1000;
                realtime_tracing_timeline = res['data'];

                dataLengthRealtime = dataLength;
                date1Realtime = date1;
                date2Realtime = date2;
                diffRealtime = diff;

                //console.log(realtime_tracing_timeline);

                $("#contact-tracing-playback-zone").kendoSlider({
                    min: 1,
                    max: diff,
                    tooltip: {
                        template: kendo.template("#= formatSliderTooltip(value) #")
                    },
                    change: playbackZoneOnChange,
                    slide: playbackZoneOnSlide
                });
            }
        }
    });
}


var TV;
var resBack = [];
var dataRealtimeBack = [];

function contactTracingCec(uuid = "", dateString = "") {
    var val = $("#contact-tracing-userId-cec").val();
    if ((uuid == "" || uuid == undefined) && val != undefined && val != "") {
        uuid = val;
    }
    if (uuid == "" || uuid == undefined) {
        uuid = null;
    }

    //start load new content realtime
    if ($("#dateContactTracingCec").data("kendoCalendar") != undefined) {
        var dateContactTracing = $("#dateContactTracingCec").data("kendoCalendar").value();
        var dateString = kendo.toString(dateContactTracing, "yyyy-MM-dd HH:mm:ss");
        var dateHeader = kendo.toString(dateContactTracing, "dd-MM-yyyy");
    }
    if (dateString == null) {
        kendo.alert("Date is empty.");
    }
    if (uuid == null) {
        kendo.alert("Person ID is empty.");
    }

    setuuid = val;
    setdate = dateString;
    
    //fungai dPET list time
    $.ajax({
        type: 'POST',
        url: base_url + 'Widget/SmartwatchDataRoomDetail?uuid=' + uuid + '&&startdate=' + dateString,
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function () {
        },
        success: function (res) {
			resBack.push(res);
            // check data null
            if (res['data'].length < 1) {
                kendo.alert("Data With uuid " + uuid + " not found.");
            } else {
                tracing_zone = [];
                dataRealtimeBack = [];
                // Set Collor Visited Zone 
                for (var i = 0; i < res['VisitedZone'].length; i++) {
                    tracing_zone.push(res['VisitedZone'][i]._id.toString());
                }

                removeBox("alert");
                clearPlayback();
                $(".button-icon-contact-tracing").addClass("active");
                $("#devicebox-button-contact-tracing img").attr("src", base_url + "Content/assets/images/contact-tracing-active.png");
                $("#box-alert").html("");
                $("#box-sensor").html("");
                $("#box-human-presence").html("");
                $("#windowPopup").data("kendoWindow").close();
                filter_tracing_active_cec = true;

                human_building_cec();

                var _itemTemplate = kendo.template($("#tmpl-WidgetRealtimeContactTracing").html(), { useWithBlock: false });
                jres = res['data'];
                resdata = [];
                for (var i = 0; i < jres.length; i++) {
                    var milli = jres[i].RecordTimestamp.replace(/\/Date\((-?\d+)\)\//, '$1');
                    var date = new Date(parseInt(milli));
                    var datetext = kendo.toString(date, "HH:mm");
                    var datetext2 = kendo.toString(date, "yyyy-MM-dd HH:mm:ss");
                    var j = {
                        RecordTimestamp: datetext,
                        Time: datetext2,
                        location: jres[i].location,
                        locationID: jres[i].locationID,
                        personID: jres[i].personID,
                        personName: jres[i].personName,
                        status: jres[i].status,
                        top: jres[i].top,
                        left: jres[i].left,
                        _id: jres[i]._id,
                        inRoom: jres[i].inRoom,
                        floor: jres[i].location.slice(0, 3).slice(1),
                    };
                    resdata.push(j);
                    realtime_tracing_timeline.push(j);

                    if (setuuid == "") {
                        setuuid = jres[0].personID;
                    }
                    if (setlocID == "") {
                        setlocID = jres[0].locationID;
                    }
                }
                var TV = res['VisitedZone'].length;
                let data_realtime = {
                    TotalVisited: TV,
                    TotalContacted: res['TotalContacted'],
                    uuid: res['uuid'],
                    personName: res['personName'],
                    data: resdata,
                    colorZone: res['allZone'],
                    date: kendo.toString(resdata[0].Time, "yyyy-MM-dd HH:mm:ss")
                };
                dataRealtimeBack.push(data_realtime);
                var result = _itemTemplate(data_realtime); //Pass the data to the compiled template
                $("#wrapper-widget-realtime .widget-content").html(result);
                $("#wrapper-widget-realtime .widget-footer").addClass("hide");
                var today = kendo.toString(new Date(), "dd-MM-yyyy");
                $(".widget-realtime-content .title-realtime").html("Contact Tracing - " + dateHeader);
                $(".realtime-ContactTracing").parent().css("max-height", "525px");
                $(".realtime-ContactTracing").closest(".widget-realtime-content").css("width", "420px");

                if ($("#dateContactTracing").data("kendoDatePicker") != undefined) {
                    var dateContactTracing = $("#dateContactTracing").data("kendoDatePicker").value();
                    var dateString = kendo.toString(dateContactTracing, "dd-MM-yyyy HH:mm:ss");
                    $("#headingRealtime span.title-realtime").html("Contact Tracing - " + dateHeader);
                    //console.log(dateString);
                }

                //end load new content realtime

                //show playback
                var _itemPlaybackTracing = kendo.template($("#tmpl-WidgetFloatingPlaybackContactTracingCEC").html(), { useWithBlock: false });
                var resultPlaybackTracing = _itemPlaybackTracing({});
                $("#box-playback-tracing").html(resultPlaybackTracing);

                var dataLength = res['data'].length;

                var milli2 = res['data'][0].RecordTimestamp.replace(/\/Date\((-?\d+)\)\//, '$1');
                var date2 = new Date(parseInt(milli2));
                var datetext2 = kendo.toString(date2, "yyyy-MM-dd HH:mm:ss");

                var milli3 = res['data'][dataLength - 1].RecordTimestamp.replace(/\/Date\((-?\d+)\)\//, '$1');
               
                var date3 = new Date(parseInt(milli3));
                var datetext3 = kendo.toString(date3, "yyyy-MM-dd HH:mm:ss");

                var date1 = new Date(datetext2);
                var date2 = new Date(datetext3);
                var diff = (date2.getTime() - date1.getTime()) / 1000;
                realtime_tracing_timeline = res['data'];

                dataLengthRealtime = dataLength;
                date1Realtime = date1;
                date2Realtime = date2;
                diffRealtime = diff;

                //console.log(realtime_tracing_timeline);

                $("#contact-tracing-playback-zone").kendoSlider({
                    min: 1,
                    max: diff,
                    tooltip: {
                        template: kendo.template("#= formatSliderTooltip(value) #")
                    },
                    change: playbackZoneOnChange,
                    slide: playbackZoneOnSlide
                });
            }
        }
    });
}

function contactTracingCecBack() {
    human_building_cec();
    tracing_zone = [];
    for (var i = 0; i < resBack[0]['VisitedZone'].length; i++) {
        tracing_zone.push(resBack[0]['VisitedZone'][i]._id.toString());
    }

    $(".button-icon-contact-tracing").addClass("active");
    $("#devicebox-button-contact-tracing img").attr("src", base_url + "Content/assets/images/contact-tracing-active.png");
    $("#box-alert").html("");
    $("#box-sensor").html("");
    $("#box-human-presence").html("");
    $("#windowPopup").data("kendoWindow").close();
    filter_tracing_active_cec = true;


    var _itemTemplate = kendo.template($("#tmpl-WidgetRealtimeContactTracing").html(), { useWithBlock: false });
    var result = _itemTemplate(dataRealtimeBack[0]); //Pass the data to the compiled template
    $("#wrapper-widget-realtime .widget-content").html(result);
    $("#wrapper-widget-realtime .widget-footer").addClass("hide");
}

function contactTracingDetail(id, uuid) {
    $.ajax({
        type: 'POST',
        url: base_url + 'Widget/SmartwatchDataRoomContacted?id=' + id + '&&uuid=' + uuid,
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function () {
        },
        success: function (res) {
	        clear_all_avatar();
	        set_all_default_zone();
            setColorByZoneTopOnly(res.location.toLowerCase(), color_global.yellow, "");

            dateString = kendo.toString(new Date(res.timestamp), "yyyy-MM-dd HH:mm:ss");

            var avatar = gltf.scene.children.find(f => f.name == res.avatarID.toLowerCase().trim());
            if (avatar != undefined) {
                avatar.visible = true;
                avatar.material.visible = true;
                if (res.inRoomStatus == "POSITIVE") {
                    avatar.material.color.setHex(color_global.red);
                }
	        }
	        $(".ContactTracingHeader").hide();
            $(".ContactTracingDetail").show();
            var milli = res.timestamp.replace(/\/Date\((-?\d+)\)\//, '$1');
            var date = new Date(parseInt(milli));
            $(".ContactTracingDetail .time-contact-tracing").html(kendo.toString(new Date(date),"HH:mm"));
	        $(".ContactTracingDetail .zonename-contact-tracing-title").html(res.location);
            // var a = i+1;
	        //var contactData = dummy_tracing_timeline[i].contactData;
	        var contactData = res['contactData'];

            $(".ContactTracingDetail .zonename-contact-tracing-index").html(res.inRoom);
            $(".ContactTracingDetail .persons-contact-tracing-title span").html(contactData.length);
	        var html = "";
            for (var c = 0; c < contactData.length; c++){
                var milli = contactData[c].RecordTimestamp.replace(/\/Date\((-?\d+)\)\//, '$1');
                var date = new Date(parseInt(milli));
                var tittleTime = (contactData[c].status == "POSITIVE") ? 'title="' + kendo.toString(date, "dd-MM-yyyy HH:mm:ss") +'"': "";
                var classHighSkin = (contactData[c].status == "POSITIVE") ? "bg-contact-tracing-danger": "bg-contact-tracing-normal";
                var icon_user = (contactData[c].status == "POSITIVE") ? base_url + 'Content/assets/images/icon-user-danger.png' : base_url + 'Content/assets/images/icon-user.png';
	        	html += '<li>'+
                    "<a href=\"javascript:void(0);\" " + tittleTime +" onclick=\"contactTracingIndividual('"+contactData[c].personID+"')\">"+
	        					'<img src="'+icon_user+'" width="30" class="icon-contact-tracing"/>'+
	        					'<span class="personid-contact-tracing '+classHighSkin+'">'+ contactData[c].personID +'</span>'+
	        					'<span class="personname-contact-tracing '+classHighSkin+'">'+ contactData[c].personName +'</span>'+
	        				'</a>'+
	        			'</li>';
	        }
	        $(".persons-contact-tracing").html(html);
        }
    });
}
function contactTracingIndividual(user) {
    openWindow('windowContactTracingCec',[]);
    $("#contact-tracing-userId").val(user);
    $("#contact-tracing-userId-cec").val(user);
}

var playBackSpeed = [
    1000,//1s
    60000,//1min
    120000,//2min
    240000,//4min
    480000,//8min
];

/**
 * VARIABLE PLAYBACK POPUP
 */
var playTime;
var currentSpeed = playBackSpeed[0];//1s
var delayTime = playBackSpeed[0];//1s
var currentTimePlay;

function playbackOnChange(ev) {
    //console.log("OnChange");
    //var evOnTime = new Date(dummy_tracing_timeline[0].Time).getTime() + (ev.value * delayTime);
    //var check = dummy_tracing_timeline.filter(f => new Date(f.Time) <= new Date(evOnTime));
    var evOnTime = new Date(resdata[0].Time).getTime() + (ev.value * delayTime);
    var check = resdata.filter(f => new Date(f.Time) <= new Date(evOnTime));
    var windows_title = "Contact Tracing - " + kendo.toString(new Date(evOnTime), "dd-MM-yyyy HH:mm:ss");
    $("#windowPopup").data("kendoWindow").title(windows_title);

    if (check.length > 0) {
        var curTime = new Date(check[0].Time);
        var idx = 0;

        check.map(function (temp, i) {
            if (new Date(temp.Time) > curTime) {
                curTime = new Date(temp.Time);
                idx = i;
            }
        });
        var top = resdata[idx].top;
        var left = resdata[idx].left;
        $(".person-icon-playback").attr("style", "left: " + left + "; top: " + top);
        $("#zoneFloor").text("Floor " + resdata[idx].floor);

        var imageBackground = (resdata[idx].floor == "03") ? websiteUrl + 'Content/assets/digitaltwin/img/f03_map.png' : websiteUrl + 'Content/assets/digitaltwin/img/f07_map.jpg';
        $(".popup-contact-tracing-playback").css("background-image", "url(" + imageBackground + ")");

        var imageIconTracing = (resdata[idx].status == "POSITIVE") ? websiteUrl + 'Content/assets/images/icon-user-danger.png' : websiteUrl + 'Content/assets/images/icon-user.png';
        $(".person-icon-playback").attr("src", imageIconTracing);


        $("#zoneName").text("Zone Name: " + resdata[idx].location);
        $("#zoneDate").text("Date: " + kendo.toString(new Date(resdata[idx].Time), "dd-MM-yyyy"));
        contactTracingDetail(resdata[idx]._id, resdata[idx].personID);
    }
}


function playbackOnSlide(ev) {
    //console.log("OnSlide");
    //var date1 = new Date(dummy_tracing_timeline[0].Time);
    var date1 = new Date(resdata[0].Time);
    var newCurrent = date1.getTime() + (ev.value * delayTime);
    currentTimePlay = new Date(newCurrent);
    var lastValue = (currentTimePlay.getTime() - date1.getTime()) / 1000;
    $("#popup-contact-tracing-playback-action").data("kendoSlider").value(lastValue);
    $("#popup-contact-tracing-playback-action").data("kendoSlider").trigger("change", { value: lastValue });
}

function playbackButton() {
    var playCmd = $(".popup-contact-tracing-playback-button a i#playButton").hasClass("fa-play");
    //contactTracingDetail(0);
    contactTracingDetail(resdata[0]._id, resdata[0].personID);
	if(playCmd){
        //var dataLength = dummy_tracing_timeline.length;
        //var date1 = new Date(dummy_tracing_timeline[0].Time);
        //var date2 = new Date(dummy_tracing_timeline[dataLength - 1].Time);
        //var dataLength = dataLengthRealtime;

        var dataLength = resdata.length;
        var date1 = new Date(resdata[0].Time);
        var date2 = new Date(resdata[dataLength - 1].Time);
        currentTimePlay = date1;
        playTime = setInterval(function(){
            const currentToTime = new Date(currentTimePlay);
            if (currentToTime < date2) {
                currentTimePlay = new Date(currentTimePlay).setMilliseconds(currentSpeed);
                var windows_title = "Contact Tracing - "+kendo.toString(new Date(currentTimePlay),"dd-MM-yyyy HH:mm:ss");
                $("#windowPopup").data("kendoWindow").title(windows_title);

                $("#zoneFloor").text("Floor " + resdata[0].floor);

                var imageBackground = (resdata[0].floor == "03") ? websiteUrl + 'Content/assets/digitaltwin/img/f03_map.png' : websiteUrl + 'Content/assets/digitaltwin/img/f07_map.jpg';
                $(".popup-contact-tracing-playback").css("background-image", "url(" + imageBackground + ")");

                $("#zoneName").text("Zone Name: " + resdata[0].location);
                $("#zoneDate").text("Date: " + kendo.toString(new Date(resdata[0].Time), "dd-MM-yyyy"));

                var currentTime = new Date(currentTimePlay);
                var lastValue = (currentTime.getTime() - date1.getTime())/1000;
                $("#popup-contact-tracing-playback-action").data("kendoSlider").value(lastValue);
                $("#popup-contact-tracing-playback-action").data("kendoSlider").trigger("change", { value: lastValue });
            }else{
                playbackButton();
                //clearInterval(playTime);
                //currentTimePlay = undefined;
            }
            
        },delayTime);
		// for(var i = 0; i < dummy_position_avatar.length;i++){
		// 	playbackSet(i);
		// }
		$(".popup-contact-tracing-playback-button a i#playButton").removeClass("fa-play");
		$(".popup-contact-tracing-playback-button a i#playButton").addClass("fa-stop");
	}else{
		clearTimeout(playTime);
        var windows_title = "Contact Tracing - " + kendo.toString(new Date(resdata[0].Time),"dd-MM-yyyy HH:mm:ss");
		$("#windowPopup").data("kendoWindow").title(windows_title);
		$(".popup-contact-tracing-playback-button a i#playButton").removeClass("fa-stop");
		$(".popup-contact-tracing-playback-button a i#playButton").addClass("fa-play");
		$("#popup-contact-tracing-playback-action").data("kendoSlider").value(1);
		$("#popup-contact-tracing-playback-action").data("kendoSlider").trigger("change", { value: 1 });
	}
}
function playbackForwardButton() {
    var playCmd = $(".popup-contact-tracing-playback-button a i#playButton").hasClass("fa-play");
	if(!playCmd && currentSpeed >= playBackSpeed[0] && currentSpeed <= playBackSpeed[4]){
        var currentIndexSpeed = playBackSpeed.findIndex(f=> f == currentSpeed);
        if(currentIndexSpeed < 4){
            currentSpeed = playBackSpeed[currentIndexSpeed + 1];
            $("#current-speed").html("SPEED: x" + (currentIndexSpeed + 1));
            $(".speed-desc p").html("Playback Speed x" + (currentIndexSpeed + 1) + " (1sec = "+ (currentSpeed/60000) +"min)");
        }else{
            currentSpeed = playBackSpeed[4];
            $("#current-speed").html("SPEED: x4");
            $(".speed-desc p").html("Playback Speed x4 (1sec = 8min)");
        }
    }
}
function playbackBackwardButton() {
    var playCmd = $(".popup-contact-tracing-playback-button a i#playButton").hasClass("fa-play");
	if(!playCmd && currentSpeed >= playBackSpeed[0] && currentSpeed <= playBackSpeed[4]){
        var currentIndexSpeed = playBackSpeed.findIndex(f=> f == currentSpeed);
        if(currentIndexSpeed > 0){
            currentSpeed = playBackSpeed[currentIndexSpeed - 1];
            $("#current-speed").html("SPEED: x" + (currentIndexSpeed - 1));

            var min = (currentSpeed/60000) < 1 ? 1 + "sec" : (currentSpeed/60000) + "min";
            
            $(".speed-desc p").html("Playback Speed x" + (currentIndexSpeed - 1) + " (1sec = "+ min +")");
        }else{
            currentSpeed = playBackSpeed[0];
            $("#current-speed").html("SPEED: x0");
            $(".speed-desc p").html("Playback Speed x0 (1sec = 1sec)");
        }
    }
}
/**
 * VARIABLE PLAYBACK ZONE
 */
var playZoneTime;
var currentZoneSpeed = playBackSpeed[0];//1s
var delayZoneTime = playBackSpeed[0];//1s
var currentZoneTimePlay;

function formatSliderTooltip(value) {
    var date1 = new Date(resdata[0].Time).getTime() + (value * delayZoneTime);
    return kendo.toString(new Date(date1), "HH:mm");
}

function playbackZoneOnChange(ev) {
    //var evOnTime = new Date(dummy_tracing_timeline[0].Time).getTime() + (ev.value * delayZoneTime);
    //var check = dummy_tracing_timeline.filter(f => new Date(f.Time) <= new Date(evOnTime));

    var evOnTime = new Date(resdata[0].Time).getTime() + (ev.value * delayZoneTime);
    var check = resdata.filter(f => new Date(f.Time) <= new Date(evOnTime));

    var title_realtime = "Contact Tracing - "+kendo.toString(new Date(evOnTime),"dd-MM-yyyy | HH:mm:ss");
    $(".widget-realtime-content .title-realtime").html(title_realtime);

    if(check.length > 0){
        var curTime = new Date(check[0].Time);
        var idx = 0;

        check.map(function (temp, i) {
            if (new Date(temp.Time) > curTime) {
                curTime = new Date(temp.Time);
                idx = i;
            }
        });
        //zone change color
        contactTracingDetail(resdata[idx]._id, resdata[idx].personID);
    }	
}
function playbackZoneOnSlide(ev) {
    var date1 = new Date(resdata[0].Time);
    var newCurrent = date1.getTime() + (ev.value * delayZoneTime);
    currentZoneTimePlay = new Date(newCurrent);

    var lastValue = (currentZoneTimePlay.getTime() - date1.getTime())/1000;
    $("#contact-tracing-playback-zone").data("kendoSlider").value(lastValue);
    $("#contact-tracing-playback-zone").data("kendoSlider").trigger("change", { value: lastValue });
}
function playbackZoneButton() {
    var playCmd = $(".contact-tracing-playback-zone-action a i#playZoneButton").hasClass("fa-play");
    contactTracingDetail(resdata[0]._id, resdata[0].personID);
	if(playCmd){
        /*var dataLength = dummy_tracing_timeline.length;
        var date1 = new Date(dummy_tracing_timeline[0].Time);
        var date2 = new Date(dummy_tracing_timeline[dataLength - 1].Time);*/

        var dataLength = resdata.length;
        var date1 = new Date(resdata[0].Time);
        var date2 = new Date(resdata[dataLength - 1].Time);
        currentZoneTimePlay = date1;
        playZoneTime = setInterval(function(){
            const currentToTime = new Date(currentZoneTimePlay);
            if(currentToTime < date2){
                currentZoneTimePlay = new Date(currentZoneTimePlay).setMilliseconds(currentZoneSpeed);
                console.log(currentZoneTimePlay);
                var title_realtime = "Contact Tracing - "+kendo.toString(new Date(currentZoneTimePlay),"dd-MM-yyyy | HH:mm:ss");
                $(".widget-realtime-content .title-realtime").html(title_realtime);

                var currentTime = new Date(currentZoneTimePlay);
                var lastValue = (currentTime.getTime() - date1.getTime()) / 1000;
                $("#contact-tracing-playback-zone").data("kendoSlider").value(lastValue);
                $("#contact-tracing-playback-zone").data("kendoSlider").trigger("change", { value: lastValue });
            }else{
                playbackZoneButton();
                //clearInterval(playZoneTime);
                //currentZoneTimePlay = undefined;
            }
            
        },delayZoneTime);
		 /*for(var i = 0; i < dummy_position_avatar.length;i++){
		 	playbackSet(i);
		 }*/
		$(".contact-tracing-playback-zone-action a i#playZoneButton").removeClass("fa-play");
		$(".contact-tracing-playback-zone-action a i#playZoneButton").addClass("fa-stop");
	}else{
        clearTimeout(playZoneTime);
        
		$(".contact-tracing-playback-zone-action a i#playZoneButton").removeClass("fa-stop");
		$(".contact-tracing-playback-zone-action a i#playZoneButton").addClass("fa-play");
		$("#contact-tracing-playback-zone").data("kendoSlider").value(1);
		$("#contact-tracing-playback-zone").data("kendoSlider").trigger("change", { value: 1 });
	}
}
function playbackZoneForwardButton() {
    var playCmd = $(".contact-tracing-playback-zone-action a i#playZoneButton").hasClass("fa-play");
	if(!playCmd && currentZoneSpeed >= playBackSpeed[0] && currentZoneSpeed <= playBackSpeed[4]){
        var currentIndexSpeed = playBackSpeed.findIndex(f=> f == currentZoneSpeed);
        if(currentIndexSpeed < 4){
            currentZoneSpeed = playBackSpeed[currentIndexSpeed + 1];
            $("#current-speed-zone").html("Speed: x" + (currentIndexSpeed + 1));
        }else{
            currentZoneSpeed = playBackSpeed[4];
            $("#current-speed-zone").html("Speed: x4");
        }
    }
}
function playbackZoneBackwardButton() {
    var playCmd = $(".contact-tracing-playback-zone-action a i#playZoneButton").hasClass("fa-play");
	if(!playCmd && currentZoneSpeed >= playBackSpeed[0] && currentZoneSpeed <= playBackSpeed[4]){
        var currentIndexSpeed = playBackSpeed.findIndex(f=> f == currentZoneSpeed);
        if(currentIndexSpeed > 0){
            currentZoneSpeed = playBackSpeed[currentIndexSpeed - 1];
            $("#current-speed-zone").html("Speed: x" + (currentIndexSpeed - 1));
        }else{
            currentZoneSpeed = playBackSpeed[0];
            $("#current-speed-zone").html("Speed: x0");
        }
    }
}

function clearPlayback() {
    clearTimeout(playTime);
    currentTimePlay = undefined;
    currentSpeed = playBackSpeed[0];
    clearTimeout(playZoneTime);
    currentZoneTimePlay = undefined;
    currentZoneSpeed = playBackSpeed[0];
}

function setPositionCec() {
    cameraCecX = camera.position.x;
    cameraCecY = camera.position.y;
    cameraCecZ = camera.position.z;
    orbitControlX = orbitControls.target.x;
    orbitControlY = orbitControls.target.y;
    orbitControlZ = orbitControls.target.z;
}

function contactTracingCecPeopleInRoom(uuid, dateString) {
    setPositionCec();
    contactTracingCec(uuid, dateString);
}