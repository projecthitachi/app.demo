﻿var data_window_popup = {};
function openWindow(category, data, options = null) {
    if (category == "Camera") {
        $("#windowPopup").data("kendoWindow").setOptions({ title: false });
        $("#windowPopup").data("kendoWindow").maximize().open();

        var height = (window.innerHeight - 60) + "px";
        $("#windowPopupBody .cctv-player-stream img").height(height);

        var _itemTemplate = kendo.template($("#tmpl-windowPopup" + category + "Realtime").html(), { useWithBlock: false });
        var result = _itemTemplate(data_window_popup[data]); //Pass the data to the compiled template
        $("#windowPopupBody").html(result);
    } else if (category == "ThermalCamera") {
        $("#windowPopup").data("kendoWindow").setOptions({ title: false });
        $("#windowPopup").data("kendoWindow").maximize().open();

        var _itemTemplate = kendo.template($("#tmpl-windowPopup" + category + "Realtime").html(), { useWithBlock: false });
        var result = _itemTemplate(data_window_popup[data]); //Pass the data to the compiled template
        $("#windowPopupBody").html(result);
    } else if (category == "ThermalCameraIR") {
        $("#windowPopup").data("kendoWindow").setOptions({ title: false });
        $("#windowPopup").data("kendoWindow").maximize().open();

        var _itemTemplate = kendo.template($("#tmpl-windowPopup" + category + "Realtime").html(), { useWithBlock: false });
        var result = _itemTemplate(data_window_popup[data]); //Pass the data to the compiled template
        $("#windowPopupBody").html(result);

    } else if (category == "ElevatorCmd") {
        $("#windowPopup").data("kendoWindow").setOptions({ width: "600px", height: "505px", title: "Elevator Command" });
        $("#windowPopup").data("kendoWindow").open().center();
        var _itemTemplate = kendo.template($("#tmpl-windowPopup" + category).html(), { useWithBlock: false });
        var result = _itemTemplate(data); //Pass the data to the compiled template
        $("#windowPopupBody").html(result);

        $("#cmdParkingDateTime, #cmdPeakOperationDateTime,#cmdStandbyDateTime").kendoDateTimePicker({
            interval: 1,
            format: "dd-MM-yyyy HH:mm",
            min: new Date(new Date().setMinutes(new Date().getMinutes() + 5))
        });
        var sch_grid = $(".cmdElv-scheduler-grid").kendoGrid({
            "dataBound": function () { },
            "columns": [
                {
                    "width": "30px",
                    "template": "<a style='width: 30px !important;min-width: 30px !important' class='k-button #= Status != 'Upcoming' ? 'd-none' : '' #' href='javascript:void(0);' onclick='schedulerOnDelete(this)' title='Delete Scheduler'><span class='fas fa-trash-alt'></span></a>"
                },
                {
                    "title": "Operation",
                    "headerAttributes": {
                        "data-field": "Operation",
                        "data-title": "Operation"
                    },
                    "width": "50px",
                    "field": "Operation",
                    "filterable": {
                        "cell": {
                            "showOperators": false,
                            "suggestionOperator": "contains",
                            "operator": "contains"
                        }
                    },
                    "encoded": true
                },
                {
                    "title": "Command",
                    "headerAttributes": {
                        "data-field": "CommandDescription",
                        "data-title": "CommandDescription"
                    },
                    "width": "100px",
                    "field": "CommandDescription",
                    "attributes": {
                        "class": "text-nowrap"
                    },
                    "filterable": {
                        "cell": {
                            "showOperators": false,
                            "suggestionOperator": "contains",
                            "operator": "contains"
                        }
                    },
                    "encoded": true
                },
                {
                    "title": "Run On",
                    "headerAttributes": {
                        "data-field": "RunOn",
                        "data-title": "RunOn"
                    },
                    "width": "70px",
                    "field": "RunOn",
                    "format": "{0:dd-MM-yyyy HH:mm}",
                    "sortable": true,
                    "filterable": {
                        "cell": {
                            "enabled": false,
                            "showOperators": false
                        }
                    },
                    "encoded": true
                }
            ],
            "pageable": {
                "refresh": true,
                "buttonCount": 10
            },
            "sortable": true,
            "selectable": "Single, Row",
            "filterable": {
                "mode": "row"
            },
            "resizable": true,
            "reorderable": true,
            "scrollable": {
                "height": "200px"
            },
            "toolbar": kendo.template($("#tmpl-windowPopupElevatorCmd-scheduler-toolbar").html()),
            "messages": {
                "noRecords": "No records available."
            },
            "dataSource": {
                "type": (function () {
                    if (kendo.data.transports['aspnetmvc-ajax']) {
                        return 'aspnetmvc-ajax';
                    } else {
                        throw new Error('The kendo.aspnetmvc.min.js script is not included.');
                    }
                })(),
                "transport": {
                    "read": {
                        "url": base_url + "Widget/ElevatorSchedulerGetList?Status=Upcoming"
                    }
                },
                "pageSize": 30,
                "page": 1,
                "total": 0,
                "serverPaging": true,
                "serverSorting": true,
                "serverFiltering": true,
                "serverGrouping": true,
                "serverAggregates": true,
                "filter": [

                ],
                "sort": [{ "field": "RunOn", "dir": "asc" }],
                "schema": {
                    "data": "Data",
                    "total": "Total",
                    "errors": "Errors",
                    "model": {
                        "id": "_id",
                        "fields": {
                            "RecordTimestamp": {
                                "type": "date",
                            },
                            "RunOn": {
                                "type": "date"
                            },
                            "Operation": {
                                "type": "string"
                            },
                            "CommandDescription": {
                                "type": "string"
                            },
                            "Status": {
                                "type": "string"
                            }
                        }
                    }
                }
            }
        });
        var sch_ddl = sch_grid.find("#scheduler-by").kendoDropDownList({
            change: function (ddl_ev) {
                $(sch_grid).data("kendoGrid").dataSource.transport.options.read.url = base_url + "Widget/ElevatorSchedulerGetList?Status=" + ddl_ev.sender._old;
                $(sch_grid).data("kendoGrid").dataSource.read();
            }
        });
        loadElevatorLastCmd();
    } else if (category.indexOf("_d-vav") > -1) {
        var _itemTemplate = kendo.template($("#tmpl-windowPopupVAV").html(), { useWithBlock: false });
        var result = _itemTemplate(data); //Pass the data to the compiled template
        $("#windowPopup").data("kendoWindow").setOptions(options);
        $("#windowPopup").data("kendoWindow").open();
        $("#windowPopupBody").html(result);
    } else if (category == "SmartPlugFloorMap") {
        var _itemTemplate = kendo.template($("#tmpl-windowPopup" + category).html(), { useWithBlock: false });
        var result = _itemTemplate(data); //Pass the data to the compiled template

        var windows_title = "";
        if (data.floor == "f03") {
            windows_title = "Map 3F";
            $("#headingRealtime span.title-realtime").html("Realtime SmartPlug - 3F");
        } else if (data.floor == "f07") {
            windows_title = "Map 7F";
            $("#headingRealtime span.title-realtime").html("Realtime SmartPlug - 7F");
        }
        $("#windowPopup").data("kendoWindow").setOptions(
            {
                title: windows_title,
                width: "1000px",
                height: "447px",
                position:
                {
                    top: 43,
                    left: 100
                },
                close: function () {
                    if (param.Menu == "SmartPlug") {
                        $("#span_location_001000000").trigger("click");
                    }
                }
            }
        );
        $("#windowPopup").data("kendoWindow").open();
        $("#windowPopupBody").html(result);
    } else if(category == "windowContactTracing"){
		var _itemTemplate = kendo.template($("#tmpl-windowPopupContactTracing").html(), { useWithBlock: false });
        var result = _itemTemplate(data); //Pass the data to the compiled template
		
		$("#windowPopup").data("kendoWindow").setOptions(
            {
                title: "Contact Tracing",
                width: "500px",
                height: "500px",
                close: function () {
					// if(filter_tracing_active){
                    //     filter_tracing_active = false;
                    //     //contactTracing();
                    //     $("#button-icon-contact-tracing").removeClass("active");
		            //     $("#devicebox-button-contact-tracing img").attr("src",base_url+"Content/assets/images/contact-tracing-dim.png");
                    // }
                }
            }
        );
        $("#windowPopup").data("kendoWindow").open().center();
        $("#windowPopupBody").html(result);
		var opt = $("#windowPopup").data("kendoWindow").options;
		opt.position.top = 65;
		$("#windowPopup").data("kendoWindow").setOptions(opt);
		$("#dateContactTracing").kendoCalendar({
			
		});
    } else if (category == "windowContactTracingCec") {
        var _itemTemplate = kendo.template($("#tmpl-windowPopupContactTracingCec").html(), { useWithBlock: false });
        var result = _itemTemplate(data); //Pass the data to the compiled template

        $("#windowPopup").data("kendoWindow").setOptions(
            {
                title: "Contact Tracing",
                width: "500px",
                height: "500px",
                close: function () {
                     //if(!filter_tracing_active){
                         //$("#button-icon-contact-tracing .box-floating-device").removeClass("active");
                        //$("#devicebox-button-contact-tracing-cec img").attr("src",base_url+"Content/assets/images/contact-tracing-dim.png");
                     //}
                }
            }
        );
        $("#windowPopup").data("kendoWindow").open().center();
        $("#windowPopupBody").html(result);
        var opt = $("#windowPopup").data("kendoWindow").options;
        opt.position.top = 65;
        $("#windowPopup").data("kendoWindow").setOptions(opt);
        $("#dateContactTracing").kendoCalendar({

        });
    } else if(category == "PlaybackContactTracing"){
		var _itemTemplate = kendo.template($("#tmpl-windowPopupContactTracingPlayback").html(), { useWithBlock: false });
        var result = _itemTemplate({}); //Pass the data to the compiled template

        var windows_title = "Contact Tracing - "+kendo.toString(new Date(dummy_tracing_timeline[0].Time),"dd-MM-yyyy HH:mm:ss");
        var widthPopup = $(".widget-close-right").offset().left;
		var heightPopup = $(".widget-analytic-wrapper > .widget").offset().top;
        $("#windowPopup").data("kendoWindow").setOptions(
            {
                title: windows_title,
                width: widthPopup - 100,
                height: heightPopup - 83,
                position:
                {
                    top: 43,
                    left: 100
                },
                open: function(){
                    clearPlayback();
                },
				close:function(){
                    clearPlayback();
                }
            }
        );
        $("#windowPopup").data("kendoWindow").open();
        $("#windowPopupBody").html(result);
        
        var dataLength = dummy_tracing_timeline.length;
        var date1 = new Date(dummy_tracing_timeline[0].Time);
        var date2 = new Date(dummy_tracing_timeline[dataLength-1].Time);
        var diff = (date2.getTime() - date1.getTime())/1000;

		$("#popup-contact-tracing-playback-action").kendoSlider({
			min: 1,
            max: diff,
            tooltip: {
                template: kendo.template("#= formatSliderTooltip(value) #")
            },
            change: playbackOnChange,
            slide: playbackOnSlide
		});
		$(".person-icon-playback").css({left: dummy_position_avatar[0].left, top: dummy_position_avatar[0].top});
    } else if (category == "ToolTip") {

    } else {
        options.title = data_window_popup.title;
        $("#windowPopup").data("kendoWindow").setOptions(options);
        $("#windowPopup").data("kendoWindow").open();
        var height = (window.innerHeight - 60) + "px";
        $("#windowPopupBody .cctv-player-stream img").height(height);
        var _itemTemplate = kendo.template($("#tmpl-windowPopup" + category).html(), { useWithBlock: false });


        if (screen.width == 1366) {
            $("#windowPopup").data("kendoWindow").wrapper.width("908px");
            for (var i = 0; i < data.data.length; i++) {
                data.data[i].Left = data.data[i].Left - 80;
                data.data[i].LeftTitle = 80;
            }
        }

        var result = _itemTemplate(data); //Pass the data to the compiled template
        $("#windowPopupBody").html(result);


    }
}
var data_temp_present_value = [];
function openWindowPresentValue(e) {
    if ($(e).attr("data-title").split("present-value-")[1].substr(0, 3) === "VAV") {
        var votitle = $(e).attr("data-title").split("present-value-")[1];
        var vovalue = $(e).find(".value-box").html();
        if (vovalue == "&nbsp;")
            vovalue = "";

        var poData = $(e).attr("id").split("-");
        $("#windowPopupPresentValue_wnd_title").html(votitle);
        $("#windowPopupPresentValue #cat-present-value").val($(e).attr("data-title").split("present-value-")[1].substr(0, 3));
        $("#windowPopupPresentValue #device-id-present-value").val(poData[1]);
        $("#windowPopupPresentValue #object-present-value").val(poData[2]);
        $("#windowPopupPresentValue #unit-present-value").val(poData[3]);
        $("#windowPopupPresentValue #input-present-value").val(vovalue);

    } else {
        const data = data_temp_present_value;
        //var title = data.DeviceID + '-' + $(e).attr("data-title").split("present-value-")[1];
        var title = $(e).attr("data-title").split("present-value-")[1];
        var value = $(e).find(".value-box").html();
        if (value === "&nbsp;")
            value = "";
        $("#windowPopupPresentValue_wnd_title").html(title);
        $("#windowPopupPresentValue #device-id-present-value").val(data.DeviceID);
        $("#windowPopupPresentValue #object-present-value").val(data.ObjectID);
        $("#windowPopupPresentValue #unit-present-value").val(data.Unit);
        $("#windowPopupPresentValue #input-present-value").val(value);
    }
    $("#windowPopupPresentValue").data("kendoWindow").open().center();
}
function window_popup_ahu_close(e) {
    //param.Location = "001000000";
    //param.Menu = "AirCond";
    console.log(e.sender);
    if (param.Menu == "AirCond") {
        window.popup_aircon_close = true;
        resetInitRealtime();
        setWidgetRealtime();
        setWidgetAnalytic();
        draggableSetView();
    }
}
function closeWindow(e) {
    $("#windowPopupPresentValue").data("kendoWindow").close();
}

function base64ArrayBuffer(arrayBuffer) {
    var base64 = '';
    var encodings = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

    var bytes = new Uint8Array(arrayBuffer);
    var byteLength = bytes.byteLength;
    var byteRemainder = byteLength % 3;
    var mainLength = byteLength - byteRemainder;

    var a, b, c, d;
    var chunk;

    // Main loop deals with bytes in chunks of 3
    for (var i = 0; i < mainLength; i = i + 3) {
        // Combine the three bytes into a single integer
        chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2];

        // Use bitmasks to extract 6-bit segments from the triplet
        a = (chunk & 16515072) >> 18; // 16515072 = (2^6 - 1) << 18
        b = (chunk & 258048) >> 12; // 258048   = (2^6 - 1) << 12
        c = (chunk & 4032) >> 6; // 4032     = (2^6 - 1) << 6
        d = chunk & 63;               // 63       = 2^6 - 1

        // Convert the raw binary segments to the appropriate ASCII encoding
        base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d];
    }

    // Deal with the remaining bytes and padding
    if (byteRemainder == 1) {
        chunk = bytes[mainLength];

        a = (chunk & 252) >> 2; // 252 = (2^6 - 1) << 2

        // Set the 4 least significant bits to zero
        b = (chunk & 3) << 4; // 3   = 2^2 - 1

        base64 += encodings[a] + encodings[b] + '==';
    } else if (byteRemainder == 2) {
        chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1];

        a = (chunk & 64512) >> 10; // 64512 = (2^6 - 1) << 10
        b = (chunk & 1008) >> 4; // 1008  = (2^6 - 1) << 4

        // Set the 2 least significant bits to zero
        c = (chunk & 15) << 2; // 15    = 2^4 - 1

        base64 += encodings[a] + encodings[b] + encodings[c] + '=';
    }

    return base64;
}
function window_camera_close(e) {
    console.log(e);
}
function closeStreaming() {
    $("#windowPopup").data("kendoWindow").close();
    $("#windowPopupBody").html("");
}
function popupCameraPlayback(data) {
    var height = (window.innerHeight - 60) + "px";
    $("#windowPopupBody .cctv-player-stream img").height(height);

    var _itemTemplate = kendo.template($("#tmpl-windowPopupCameraPlayback").html(), { useWithBlock: false });
    var result = _itemTemplate(data); //Pass the data to the compiled template
    $("#windowPopupBody").html(result);

    $("#camera-playback-calendar").kendoCalendar();
    $("#playback-timepicker-from").kendoTimePicker();
    $("#playback-timepicker-to").kendoTimePicker();
}
function popupCameraRealtime(data) {
    var height = (window.innerHeight - 60) + "px";
    $("#windowPopupBody .cctv-player-stream img").height(height);

    var _itemTemplate = kendo.template($("#tmpl-windowPopupCameraRealtime").html(), { useWithBlock: false });
    var result = _itemTemplate(data); //Pass the data to the compiled template
    $("#windowPopupBody").html(result);
}
function popupThermalCameraPlayback(data) {
    //var height = (window.innerHeight - 60) + "px";
    //$("#windowPopupBody .cctv-player-stream img").height(height);

    var _itemTemplate = kendo.template($("#tmpl-windowPopupThermalCameraPlayback").html(), { useWithBlock: false });
    var result = _itemTemplate(data); //Pass the data to the compiled template
    $("#windowPopupBody").html(result);

    $("#camera-playback-calendar").kendoCalendar();
    $("#playback-timepicker-from").kendoTimePicker();
    $("#playback-timepicker-to").kendoTimePicker();
}
function popupThermalCameraRealtime(data) {
    //var height = (window.innerHeight - 60) + "px";
    //$("#windowPopupBody .cctv-player-stream img").height(height);

    var _itemTemplate = kendo.template($("#tmpl-windowPopupThermalCameraRealtime").html(), { useWithBlock: false });
    var result = _itemTemplate(data); //Pass the data to the compiled template
    $("#windowPopupBody").html(result);
}
function popupThermalCameraIRPlayback(data) {
    //var height = (window.innerHeight - 60) + "px";
    //$("#windowPopupBody .cctv-player-stream img").height(height);

    var _itemTemplate = kendo.template($("#tmpl-windowPopupThermalCameraIRPlayback").html(), { useWithBlock: false });
    var result = _itemTemplate(data); //Pass the data to the compiled template
    $("#windowPopupBody").html(result);

    $("#camera-playback-calendar").kendoCalendar();
    $("#playback-timepicker-from").kendoTimePicker();
    $("#playback-timepicker-to").kendoTimePicker();
}
function popupThermalCameraIRRealtime(data) {
    //var height = (window.innerHeight - 60) + "px";
    //$("#windowPopupBody .cctv-player-stream img").height(height);

    var _itemTemplate = kendo.template($("#tmpl-windowPopupThermalCameraIRRealtime").html(), { useWithBlock: false });
    var result = _itemTemplate(data); //Pass the data to the compiled template
    $("#windowPopupBody").html(result);
}
function loadIframe(e) {
    console.log(e);
}
function pad(num, size) {
    var s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
}
function toBinary(x) {
    x = x.toString(2);
    return pad(x, 8);
}
String.prototype.replaceAt = function (index, replacement) {
    return this.substr(0, index) + replacement + this.substr(index + replacement.length);
}
function openAlert(data) {
    var data_temp = JSON.parse($(data).attr("data-temp"));
    var _itemTemplate = kendo.template($("#tmpl-WidgetFloatingAlert").html(), { useWithBlock: false });
    var result = _itemTemplate(data_temp); //Pass the data to the compiled template
    $("#box-alert").html(result);
    window.isOpenAlert = true;
    set_all_default_zone();
	var obj = gltf.scene.children.find(f=>f.name == "f03_wall");
		obj.material.transparent = true;
		obj.material.opacity = 0.3;
		obj.material.color.setHex(color_global.white);
    setTimeout(function () {
        setColorByZone(data_temp.locationID.toLowerCase(), color_global.yellow, "");
    }, 300);

    draggableSetView();
}
function openAlertCec(data) {
    var _itemTemplate = kendo.template($("#tmpl-WidgetFloatingAlertCEC").html(), { useWithBlock: false });
    var result = _itemTemplate(data); //Pass the data to the compiled template
    $("#box-alert").html(result);
    window.isOpenAlert = true;
    set_all_default_zone();

    draggableSetView();
}
function diff_minutes(dt2, dt1) {

    var diff = (dt2.getTime() - dt1.getTime()) / 1000;
    diff /= 60;
    return Math.abs(Math.round(diff));

}
function onDrag(event, elem) {
    if (event.x > 0 && event.y > 0) {
        $(elem).css("left", event.x + "px");
        $(elem).css("top", event.y + "px");

        const elementData = { element_id: $(elem).attr("id"), left: event.x, top: event.y };
        let dragStorage = localStorage.getItem("draggableElement");
        if (dragStorage == null)
            localStorage.setItem("draggableElement", JSON.stringify([elementData]));
        else {
            var dragStorageJson = JSON.parse(dragStorage);
            var getIdx = dragStorageJson.findIndex(f => f.element_id == $(elem).attr("id"));
            if (getIdx == -1) {
                dragStorageJson.push(elementData);
            } else {
                dragStorageJson[getIdx] = elementData;
            }
            localStorage.setItem("draggableElement", JSON.stringify(dragStorageJson));
        }
    }
}
function draggableSetView() {
    let dragStorage = localStorage.getItem("draggableElement");
    if (dragStorage != null) {
        var dragStorageJson = JSON.parse(dragStorage);
        for (var i = 0; i < dragStorageJson.length; i++) {
            $("#" + dragStorageJson[i].element_id).css("left", dragStorageJson[i].left + "px");
            $("#" + dragStorageJson[i].element_id).css("top", dragStorageJson[i].top + "px");
        }
    }
}
function dgSave() {

}
function toggleDatGui() {
    dat.GUI.toggleHide();
}


/* parking variable */
let dListCmdParking = [];
let binCmdParking = toBinary(0);
let AddressParking = "20";
/* parking variable */

/* peakoperation variable */
let aListCmdPeakOperation = [];
let dListCmdPeakOperation = [];
let cmdPeakOperation = {
    "timeStamp": parseInt(new Date().getTime()),
    "command": []
};
const AddressUpPeak = ["81", "89", "91", "99"];
const AddressDownPeak = ["82", "8A", "92", "9A"];
/* peakoperation variable */

/* standby variable */
let aListCmdStandby = [];
let dListCmdStandby = [];
let cmdStandbyValidation = {
    canSend: false,
    msg: "",
    RunOn: null,
    Operation: "",
    cmdDescription: ""
};
let cmdParkingOperationValidation = {
    canSend: false,
    msg: "",
    RunOn: null,
    Operation: "",
    cmdDescription: ""
};
let cmdPeakOperationValidation = {
    canSend: false,
    msg: "",
    RunOn: null,
    Operation: "",
    cmdDescription: ""
};
let cmdStandby = {
    "timeStamp": parseInt(new Date().getTime()),
    "command": []
};
const cmdStandbyFloor = {
    "sf1": ["41", "49", "51", "59"],
    "sf2": ["43", "4B", "53", "5B"],
    "sf3": ["45", "4D", "55", "5D"]
};
const cmdStandbyElevator = {
    "se1": ["42", "4A", "52", "5A"],
    "se2": ["44", "4C", "54", "5C"],
    "se3": ["46", "4E", "56", "5E"]
};
/* standby variable */


function elvCmdSend(el) {
    var cmdId = $(el).closest(".wrapper-has-nav-content").attr("id");
    var cmdType = $(el).attr("data-cmd");
    var url = "";
    if (cmdType == "immediately") {
        url = base_url + 'Widget/ElevatorCommands';
    } else if (cmdType == "scheduler") {
        url = base_url + 'Widget/ElevatorSchedulerSet';
    }
    if (cmdId == "cmdParking") {
        getCommandParking(0, cmdType);
        if (cmdParkingOperationValidation.canSend) {
            var voData = {
                aList: [AddressParking],
                dList: dListCmdParking,
                RunOn: cmdParkingOperationValidation.RunOn,
                Operation: cmdParkingOperationValidation.Operation,
                cmdDescription: cmdParkingOperationValidation.cmdDescription
            };
            $.ajax({
                type: 'POST',
                url: url,
                data: JSON.stringify(voData),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                beforeSend: function () {
                    $(el).attr("disabled");
                },
                success: function (result) {
                    alert(result.msg);
                    $(el).removeAttr("disabled");
                },
                error: function (xmlhttprequest, textstatus, message) {

                }
            });
        } else {
            alert(cmdParkingOperationValidation.msg);
        }
    } else if (cmdId == "cmdPeakOperation") {
        getCommandPeakOperation(0, cmdType);
        if (cmdPeakOperationValidation.canSend) {
            var voData = {
                aList: aListCmdPeakOperation,
                dList: dListCmdPeakOperation,
                RunOn: cmdPeakOperationValidation.RunOn,
                Operation: cmdPeakOperationValidation.Operation,
                cmdDescription: cmdPeakOperationValidation.cmdDescription
            };
            $.ajax({
                type: 'POST',
                url: url,
                data: JSON.stringify(voData),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                beforeSend: function () {
                    $(el).attr("disabled");
                },
                success: function (result) {
                    alert(result.msg);
                    $(el).removeAttr("disabled");
                },
                error: function (xmlhttprequest, textstatus, message) {

                }
            });
        } else {
            alert(cmdPeakOperationValidation.msg);
        }
    } else if (cmdId == "cmdStandby") {
        getCommandStandby(0, cmdType);
        if (cmdStandbyValidation.canSend) {
            var voData = {
                aList: aListCmdStandby,
                dList: dListCmdStandby,
                RunOn: cmdStandbyValidation.RunOn,
                Operation: cmdStandbyValidation.Operation,
                cmdDescription: cmdStandbyValidation.cmdDescription
            };
            $.ajax({
                type: 'POST',
                url: url,
                data: JSON.stringify(voData),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                beforeSend: function () {
                    $(el).attr("disabled");
                },
                success: function (result) {
                    alert(result.msg);
                    $(el).removeAttr("disabled");
                    cmdStandbyClear();
                },
                error: function (xmlhttprequest, textstatus, message) {

                }
            });
        } else {
            alert(cmdStandbyValidation.msg);
        }
    }
}
function getCommandParking(isReset, cmdType) {
    dListCmdParking = [];
    binCmdParking = toBinary(0);

    cmdParkingOperationValidation.canSend = false;
    cmdParkingOperationValidation.msg = "";
    cmdParkingOperationValidation.Operation = "";
    cmdParkingOperationValidation.RunOn = "";
    cmdParkingOperationValidation.cmdDescription = "Elevator ";

    $.each($('input[id*=cmdParkingCheckBox]:checked'), function (i, e) {
        const cbId = $(e).attr("id");
        const elv = cbId.split("cmdParkingCheckBox-")[1];
        binCmdParking = binCmdParking.replaceAt(8 - elv, '1');
        cmdParkingOperationValidation.cmdDescription += "" + elv + ",";
    });
    dListCmdParking.push(binCmdParking);
    if (isReset) {
        cmdParkingOperationValidation.canSend = true;
        cmdParkingOperationValidation.Operation = "Parking";
        cmdParkingOperationValidation.cmdDescription = "Reset";
        cmdParkingOperationValidation.RunOn = $("#cmdParkingDateTime").data("kendoDateTimePicker").value();
        if (cmdParkingOperationValidation.RunOn == null && cmdType == "scheduler") {
            cmdParkingOperationValidation.canSend = false;
            cmdParkingOperationValidation.msg = "Run on is required";
        }
    } else {
        cmdParkingOperationValidation.canSend = true;
        cmdParkingOperationValidation.Operation = "Parking";
        cmdParkingOperationValidation.RunOn = $("#cmdParkingDateTime").data("kendoDateTimePicker").value();
        if (cmdParkingOperationValidation.RunOn == null && cmdType == "scheduler") {
            cmdParkingOperationValidation.canSend = false;
            cmdParkingOperationValidation.msg = "Run on is required \n";
        }
        if (dListCmdParking[0].indexOf("1") == -1) {
            cmdParkingOperationValidation.canSend = false;
            cmdParkingOperationValidation.msg += "Select elevator to send command.";
        }
    }
}
function getCommandPeakOperation(isReset, cmdType) {
    aListCmdPeakOperation = [];
    dListCmdPeakOperation = [];
    cmdPeakOperation.timeStamp = parseInt(new Date().getTime());
    cmdPeakOperation.command = [];
    cmdPeakOperationValidation.canSend = false;
    cmdPeakOperationValidation.msg = "";
    cmdPeakOperationValidation.Operation = "";
    cmdPeakOperationValidation.RunOn = "";
    cmdPeakOperationValidation.cmdDescription = "";

    const TypeSelected = $(".cmdPeakOperation-label img.active").attr("data-value");
    const Floor = $("#cmdPeakOperationFloor li.active").attr("value");
    const RunOn = $("#cmdPeakOperationDateTime").data("kendoDateTimePicker").value();

    if (TypeSelected == undefined) { cmdPeakOperationValidation.msg += "Please select Up/Down.\n"; }
    if (!isReset && Floor == undefined) { cmdPeakOperationValidation.msg += "Please select floor.\n"; }
    if (RunOn == null && cmdType == "scheduler") { cmdPeakOperationValidation.msg += "Run on is required"; }

    var idx_up_peak = AddressUpPeak.indexOf(TypeSelected);
    var idx_down_peak = AddressDownPeak.indexOf(TypeSelected);

    if (idx_up_peak > -1) {
        for (var i = 0; i < AddressUpPeak.length; i++) {
            const cmd_temp = {
                a: parseInt("0x" + AddressUpPeak[i]),
                d: isReset ? 0 : parseInt(Floor)
            };
            cmdPeakOperation.command.push(cmd_temp);
            aListCmdPeakOperation.push(AddressUpPeak[i]);
            dListCmdPeakOperation.push(toBinary(isReset ? 0 : parseInt(Floor)));
        }

        if (isReset) {
            cmdPeakOperationValidation.Operation += "Up Peak";
            cmdPeakOperationValidation.cmdDescription = "Reset";
        } else {
            cmdPeakOperationValidation.Operation += "Up Peak";
            cmdPeakOperationValidation.cmdDescription = "Floor " + Floor;
        }

    } else if (idx_down_peak > -1) {
        for (var i = 0; i < AddressDownPeak.length; i++) {
            const cmd_temp = {
                a: parseInt("0x" + AddressDownPeak[i]),
                d: isReset ? 0 : parseInt(Floor)
            };
            cmdPeakOperation.command.push(cmd_temp);
            aListCmdPeakOperation.push(AddressDownPeak[i]);
            dListCmdPeakOperation.push(toBinary(isReset ? 0 : parseInt(Floor)));
        }
        if (isReset) {
            cmdPeakOperationValidation.Operation += "Down Peak";
            cmdPeakOperationValidation.cmdDescription = "Reset";
        } else {
            cmdPeakOperationValidation.Operation += "Down Peak";
            cmdPeakOperationValidation.cmdDescription = "Floor " + Floor;
        }
    }

    if (cmdPeakOperationValidation.msg.length > 0) {
        cmdPeakOperationValidation.canSend = false;
    } else {
        cmdPeakOperationValidation.RunOn = RunOn;
        cmdPeakOperationValidation.canSend = true;
    }
}
function elvCmdReset(el) {
    var cmdId = $(el).closest(".wrapper-has-nav-content").attr("id");
    var cmdType = $(el).attr("data-cmd");
    var url = "";
    if (cmdType == "immediately") {
        url = base_url + 'Widget/ElevatorCommands';
    } else if (cmdType == "scheduler") {
        url = base_url + 'Widget/ElevatorSchedulerSet';
    }
    if (cmdId == "cmdParking") {
        getCommandParking(1, cmdType);
        if (cmdParkingOperationValidation.canSend) {
            var voData = {
                aList: [AddressParking],
                dList: [toBinary(0)],
                RunOn: cmdParkingOperationValidation.RunOn,
                Operation: cmdParkingOperationValidation.Operation,
                cmdDescription: cmdParkingOperationValidation.cmdDescription
            };
            $.ajax({
                type: 'POST',
                url: url,
                data: JSON.stringify(voData),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                beforeSend: function () {
                    $(el).attr("disabled");
                },
                success: function (result) {
                    alert(result.msg);
                    $(el).removeAttr("disabled");
                },
                error: function (xmlhttprequest, textstatus, message) {

                }
            });
        } else {
            alert(cmdParkingOperationValidation.msg);
        }
    } else if (cmdId == "cmdPeakOperation") {
        getCommandPeakOperation(1, cmdType);
        if (cmdPeakOperationValidation.canSend) {
            var voData = {
                aList: aListCmdPeakOperation,
                dList: dListCmdPeakOperation,
                RunOn: cmdPeakOperationValidation.RunOn,
                Operation: cmdPeakOperationValidation.Operation,
                cmdDescription: cmdPeakOperationValidation.cmdDescription
            };
            $.ajax({
                type: 'POST',
                url: url,
                data: JSON.stringify(voData),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                beforeSend: function () {
                    $(el).attr("disabled");
                },
                success: function (result) {
                    alert(result.msg);
                    $(el).removeAttr("disabled");
                },
                error: function (xmlhttprequest, textstatus, message) {

                }
            });
        } else {
            alert(cmdPeakOperationValidation.msg);
        }
    } else if (cmdId == "cmdStandby") {
        cmdStandbyClear();
        for (var i = 1; i <= 3; i++) {
            getCommandStandby(1, cmdType);
        }
        if (cmdStandbyValidation.canSend) {
            var voData = {
                aList: aListCmdStandby,
                dList: dListCmdStandby,
                RunOn: cmdStandbyValidation.RunOn,
                Operation: cmdStandbyValidation.Operation,
                cmdDescription: cmdStandbyValidation.cmdDescription
            };
            $.ajax({
                type: 'POST',
                url: url,
                data: JSON.stringify(voData),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                beforeSend: function () {
                    $(el).attr("disabled");
                },
                success: function (result) {
                    alert(result.msg);
                    $(el).removeAttr("disabled");
                    cmdStandbyClear();
                },
                error: function (xmlhttprequest, textstatus, message) {

                }
            });
        } else {
            alert(cmdStandbyValidation.msg);
        }
    }
}
function elvCmdPreview(el) {
    var cmdId = $(el).closest(".wrapper-has-nav-content").attr("id");
    if (cmdId == "cmdParking") {
        getCommandParking();
        var cmd = {
            "timeStamp": parseInt(new Date().getTime()),
            "command": [{
                "a": parseInt("0x20"),
                "d": parseInt(binCmdParking, 2)
            }]
        };
        $("#" + cmdId + " #txt-preview").val(JSON.stringify(cmd, undefined, 4));
    } else if (cmdId == "cmdPeakOperation") {
        getCommandPeakOperation(0);
        $("#" + cmdId + " #txt-preview").val(JSON.stringify(cmdPeakOperation, undefined, 4));
    } else if (cmdId == "cmdStandby") {
    }
}
function getCommandStandby(isReset, cmdType) {
    aListCmdStandby = [];
    dListCmdStandby = [];
    cmdStandbyValidation.canSend = false;
    cmdStandbyValidation.msg = "";
    cmdStandbyValidation.Operation = "";
    cmdStandbyValidation.RunOn = "";
    cmdStandbyValidation.cmdDescription = "";

    cmdStandby.timeStamp = parseInt(new Date().getTime());
    var elvSummary = 0;
    var floorSummary = [];
    for (var x = 1; x <= 3; x++) {
        var elevator = $("#cmdStandbyElevator-" + x).val() == "" ? 0 : parseInt($("#cmdStandbyElevator-" + x).val());
        var floor = $("#cmdStandby-" + x + " li.active").attr("value");
        floor = floor == undefined ? 0 : floor;
        cmdStandbyValidation.cmdDescription += elevator + "E @" + floor + "F,";

        if (elevator > 0) {
            if (floor == 0) {
                cmdStandbyValidation.msg += "Please select floor of command-" + x + ".\n";
            } else {
                if (floorSummary.indexOf(floor) > -1)
                    cmdStandbyValidation.msg += "Floor (" + floor + ") can't redundant.";
            }
        }
        elvSummary += elevator;

        for (let i = 0; i < cmdStandbyFloor["sf" + x].length; i++) {
            const cmd_temp = {
                a: parseInt("0x" + cmdStandbyFloor["sf" + x][i]),
                d: isReset ? 0 : parseInt(floor)
            };
            cmdStandby.command.push(cmd_temp);
            aListCmdStandby.push(cmdStandbyFloor["sf" + x][i]);
            dListCmdStandby.push(toBinary(isReset ? 0 : parseInt(floor)));
        }
        for (let i = 0; i < cmdStandbyElevator["se" + x].length; i++) {
            const cmd_temp = {
                a: parseInt("0x" + cmdStandbyElevator["se" + x][i]),
                d: isReset ? 0 : parseInt(elevator)
            };
            cmdStandby.command.push(cmd_temp);
            aListCmdStandby.push(cmdStandbyElevator["se" + x][i]);
            dListCmdStandby.push(toBinary(isReset ? 0 : parseInt(elevator)));
        }
        floorSummary.push(floor);
    }

    if (!isReset & elvSummary != 4) {
        cmdStandbyValidation.msg = "Total of elevator selected must be 4.";
        aListCmdStandby = [];
        dListCmdStandby = [];
    }

    var RunOn = $("#cmdStandbyDateTime").data("kendoDateTimePicker").value();
    if (RunOn == null && cmdType == "scheduler") {
        cmdStandbyValidation.msg = "Run on is required";
        aListCmdStandby = [];
        dListCmdStandby = [];
    }

    if (cmdStandbyValidation.msg.length == 0) {
        cmdStandbyValidation.canSend = true;
        cmdStandbyValidation.RunOn = RunOn;
        cmdStandbyValidation.Operation = "Standby";
        cmdStandbyValidation.cmdDescription = isReset ? "Reset" : cmdStandbyValidation.cmdDescription;
    }

}
function cmdStandbyClear() {
    aListCmdStandby = [];
    dListCmdStandby = [];
    cmdStandbyValidation.canSend = false;
    cmdStandbyValidation.msg = "";
    cmdStandby.command = [];
}
function cmdParkingCheckboxClick(elem) {
    var elem_wrap = elem.closest(".elevator-wrapper-parking-box");
    $(elem_wrap).toggleClass("active");
}
function cmdPeakOperationClick(elem) {
    $(elem).toggleClass("active");
    var direction = $(elem).attr("data-direction");
    if (direction == "up") {
        $(".cmdPeakOperation-label-down img").attr("class", "");
        $(".cmdPeakOperation-label-down img").attr("src", img_elevator["down"]);
    } else {
        $(".cmdPeakOperation-label-up img").attr("class", "");
        $(".cmdPeakOperation-label-up img").attr("src", img_elevator["up"]);
    }
    var constant = $(elem).hasClass("active") ? direction + "_active" : direction;
    $(elem).attr("src", img_elevator[constant]);
}
function cmdPeakOperationFloorClick(elem) {
    $(".cmdPeakOperation-floor").removeClass("active");
    $(elem).toggleClass("active");
}
function cmdStandbyFloorClick(elem) {
    var elem_parent = $(elem).closest(".cmdStandby-floor-wrapper").attr("id");
    $("#" + elem_parent + " li").removeClass("active");
    $(elem).toggleClass("active");
}

function cmdStandbyElevatorKeyUp(elem) {
    var allowed = ['0', '1', '2', '3', '4'];
    var val = $(elem).val();
    if (val.length > 1 || allowed.indexOf(val) == -1) {
        $(elem).val("");
        return;
    }
    var summary = 0;
    $.each($(".cmdStandbyElevator-txt"), function (i, e) {
        summary += $(e).val() == "" ? 0 : parseInt($(e).val());
    });
    if (summary > 4) {
        $(elem).val("");
        return;
    }
}
function schedulerOnDelete(e) {
    var grid = $(".cmdElv-scheduler-grid").data("kendoGrid");
    var dataitem = grid.dataItem(e.closest("tr"));
    console.log(dataitem);
    //dataitem._id.CreationTime = new Date(dataitem._id.CreationTime.match(/\d+/)[0] * 1);

    var cnf = confirm("Are you sure you want to delete " + dataitem.Operation + " ?");
    if (cnf) {
        $.ajax({
            url: base_url + "Widget/ElevatorSchedulerDelete",
            type: 'POST',
            data: JSON.stringify({ _id: getId(dataitem._id) }),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            beforeSend: function () {
                $(e).attr("disabled", "disabled");
            },
            success: function (result) {
                $(e).removeAttr("disabled");
                alert(result.msg);
                if (result.errorcode > 0) {

                } else {
                    grid.dataSource.read();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jQuery.parseJSON(jqXHR.responseText));
            },
            async: true,
            processData: false
        });
    }
}
function loadElevatorLastCmd() {
    $.ajax({
        type: 'POST',
        url: base_url + 'Widget/ElevatorGetLastCommand',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        beforeSend: function () {

        },
        success: function (result) {
            var parking = result.parking.split("");
            for (var a = 0; a < parking.length; a++) {
                if (parking[a] == "1") {
                    $("#cmdParkingCurrentStateWrapper-" + (parking.length - a) + " .parking-current-state").addClass("active");
                } else {
                    $("#cmdParkingCurrentStateWrapper-" + (parking.length - a) + " .parking-current-state").removeClass("active");
                }
            }
            if (result.peakOperation.command == "up") {
                $(".cmdPeakOperation-label-down-current-state img").attr("src", img_elevator.down);
                $(".cmdPeakOperation-label-up-current-state img").attr("src", img_elevator.up_active);
                $(".cmdPeakOperation-floor-list-current-state li").removeClass("active");

                if (result.peakOperation.floor != "") {
                    $(".cmdPeakOperation-floor-list-current-state li[value=" + result.peakOperation.floor + "]").addClass("active");
                }
            } else if (result.peakOperation.command == "down") {
                $(".cmdPeakOperation-label-up-current-state img").attr("src", img_elevator.up);
                $(".cmdPeakOperation-label-down-current-state img").attr("src", img_elevator.down_active);
                $(".cmdPeakOperation-floor-list-current-state li").removeClass("active");

                if (result.peakOperation.floor != "") {
                    $(".cmdPeakOperation-floor-list-current-state li[value=" + result.peakOperation.floor + "]").addClass("active");
                }
            } else {
                $(".cmdPeakOperation-label-up-current-state img").attr("src", img_elevator.up);
                $(".cmdPeakOperation-label-down-current-state img").attr("src", img_elevator.down);
                $(".cmdPeakOperation-floor-list-current-state li").removeClass("active");
                if (result.peakOperation.floor != "") {
                    $(".cmdPeakOperation-floor-list-current-state li[value=" + result.peakOperation.floor + "]").addClass("active");
                }
            }

            for (var b = 1; b <= 3; b++) {
                $("#cmdStandbyCurrentState-" + b + " span.cmdStandby-current-state-elevator").html(result.standby["standby" + b].elevator + "E");
                $("#cmdStandbyCurrentState-" + b + " span.cmdStandby-current-state-floor").html(result.standby["standby" + b].floor + "F");
            }
        },
        error: function (xmlhttprequest, textstatus, message) {

        }
    });
}
function getId(mongoId) {
    var result =
        pad0(mongoId.Timestamp.toString(16), 8) +
        pad0(mongoId.Machine.toString(16), 6) +
        pad0(mongoId.Pid.toString(16), 4) +
        pad0(mongoId.Increment.toString(16), 6);

    return result;
}

function pad0(str, len) {
    var zeros = "00000000000000000000000000";
    if (str.length < len) {
        return zeros.substr(0, len - str.length) + str;
    }

    return str;
}
function getWindowWidthForContent() {
    var wScreen = $("body").width() - 100;
    var wRealtime = $(".sidebar-right").width() + 27;
    var wContent = wScreen - wRealtime;
    return wContent + "px";
}
//-- slide toggle
function maxminWindow() {
	console.log("aaa");
	$("#wrapper-widget-realtime .widget-footer").toggleClass("active");
	if ($("#wrapper-widget-realtime .widget-footer").hasClass("active")) {
		$("#wrapper-widget-realtime .widget-footer .widget-action i").attr("class", "fa fa-chevron-up");
	} else {
		$("#wrapper-widget-realtime .widget-footer .widget-action i").attr("class", "fa fa-chevron-down");
	}
}
function slideDownRealtime(){
	$("#wrapper-widget-realtime").toggleClass("active-slide");
	if ($("#wrapper-widget-realtime").hasClass("active-slide")) {
		$(".widget-realtime-close i").attr("class", "fa fa-chevron-up");
	} else {
		$(".widget-realtime-close i").attr("class", "fa fa-chevron-down");
	}
}
function toggleBox(id){
	$("#devicebox-"+id).toggleClass("active-slide");
	if ($("#devicebox-"+id).hasClass("active-slide")) {
		$("#devicebox-"+id+" .legend-toggle-box i").attr("class", "fa fa-chevron-up");
	} else {
		$("#devicebox-"+id+" .legend-toggle-box i").attr("class", "fa fa-chevron-down");
	}
}
function toggleRightAnalytic() {
	/*$(".widget-analytic-wrapper").toggleClass("active-slide");
	if ($(".widget-analytic-wrapper").hasClass("active-slide")) {
		$(".widget-analytic-close-right i").attr("class", "fa fa-chevron-left");
	} else {
		$(".widget-analytic-close-right i").attr("class", "fa fa-chevron-right");
	}*/
}
function homeToggleZone(){
	$(".home-toggle-zone").toggleClass("active");
	if ($(".home-toggle-zone").hasClass("active")) {
		$(".home-toggle-zone i").removeAttr("class");
		set_all_default_zone();
		/*var obj = gltf.scene.children.find(f=>f.name == "f03_wall");
		obj.material.transparent = true;
		obj.material.opacity = 0.3;
		obj.material.color.setHex(color_global.white);*/
	}else{
		$(".home-toggle-zone i").attr("class","fa fa-grip-horizontal");
		human_presence_persondata([]);
		//setColorByZone("f03_wall", color_global.green,"");
	}	
}
//----- DRAGABLE LEGEND
function dragElement(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (document.getElementById(elmnt.id + "header")) {
    // if present, the header is where you move the DIV from:
    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
  } else {
    // otherwise, move the DIV from anywhere inside the DIV:
    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
  }

  function closeDragElement() {
    // stop moving when mouse button is released:
    document.onmouseup = null;
    document.onmousemove = null;
  }
}

var dummy_tracing_avatar = ["f07_o-person102","f07_o-person028","f07_o-person024","f07_o-person123"];
var dummy_tracing_zone = ["f07z12","f07z06","f07z07","f07z01"];
var dummy_position_avatar = [
	{
		top: "230px",
		left: "115px",
	},
	{
		top: "80px",
		left: "505px",
	},
	{
		left: "605px",
		top: "70px"
	},
	{
		left: "720px",
		top: "300px"
	}
];
var dummy_tracing_timeline = [
			{
				ZoneName: dummy_tracing_zone[0].toUpperCase(),
				Time: "2020-08-29T07:30:00",
				IsHighSkin: false,
				contactData: [
					{
                        user: "H00001",
						name: "Person 1",
						avatar: dummy_tracing_avatar[0],
						IsHighSkin: false,
					},
					{
						user: "H00002",
						name: "Person 2",
						avatar: dummy_tracing_avatar[1],
						IsHighSkin: false,
					},
					{
						user: "H00003",
						name: "Person 3",
						avatar: dummy_tracing_avatar[2],
						IsHighSkin: false,
					},
					{
						user: "H00004",
						name: "Person 4",
						avatar: dummy_tracing_avatar[3],
						IsHighSkin: false,
					}
				]
			},
			{
				ZoneName: dummy_tracing_zone[1].toUpperCase(),
				Time: "2020-08-29T07:45:00",
				IsHighSkin: true,
				contactData: [
					{
                        user: "H00001",
						name: "Person 1",
						avatar: dummy_tracing_avatar[0],
						IsHighSkin: true,
					},
					{
						user: "H00002",
						name: "Person 2",
						avatar: dummy_tracing_avatar[1],
						IsHighSkin: false,
					},
					{
						user: "H00003",
						name: "Person 3",
						avatar: dummy_tracing_avatar[2],
						IsHighSkin: false,
					},
					{
						user: "H00004",
						name: "Person 4",
						avatar: dummy_tracing_avatar[3],
						IsHighSkin: false,
					}
				]
			},
			{
				ZoneName: dummy_tracing_zone[2].toUpperCase(),
				Time: "2020-08-29T08:45:00",
				IsHighSkin: false,
				contactData: [
					{
                        user: "H00001",
						name: "Person 1",
						avatar: dummy_tracing_avatar[0],
						IsHighSkin: false,
					},
					{
						user: "H00002",
						name: "Person 2",
						avatar: dummy_tracing_avatar[1],
						IsHighSkin: false,
					},
					{
						user: "H00003",
						name: "Person 3",
						avatar: dummy_tracing_avatar[2],
						IsHighSkin: false,
					},
					{
						user: "H00004",
						name: "Person 4",
						avatar: dummy_tracing_avatar[3],
						IsHighSkin: false,
					}
				]
			},
			{
				ZoneName: dummy_tracing_zone[3].toUpperCase(),
				Time: "2020-08-29T09:00:00",
				IsHighSkin: false,
				contactData: [
					{
                        user: "H00001",
						name: "Person 1",
						avatar: dummy_tracing_avatar[0],
						IsHighSkin: false,
					},
					{
						user: "H00002",
						name: "Person 2",
						avatar: dummy_tracing_avatar[1],
						IsHighSkin: false,
					},
					{
						user: "H00003",
						name: "Person 3",
						avatar: dummy_tracing_avatar[2],
						IsHighSkin: false,
					},
					{
						user: "H00004",
						name: "Person 4",
						avatar: dummy_tracing_avatar[3],
						IsHighSkin: false,
					}
				]
			}
		];
var filter_tracing_active = false;
function btnContactTracingIcon(e){
	if($(e).hasClass("active")){
		$(e).removeClass("active");
		$("#devicebox-button-contact-tracing img").attr("src",base_url+"Content/assets/images/contact-tracing-dim.png");
		
		$("#windowPopup").data("kendoWindow").close();
		if(filter_tracing_active){
			filter_tracing_active = false;
			setWidgetRealtime();
			clear_all_avatar();
			set_all_default_zone();
			$("#box-playback-tracing").html("");
		}

	}else{
		$(e).addClass("active");
		$("#devicebox-button-contact-tracing img").attr("src",base_url+"Content/assets/images/contact-tracing-active.png");
		openWindow('windowContactTracing',[]);		
	}	
}

var cameraCecX = 0;
var cameraCecY = 0;
var cameraCecZ = 0;
var orbitControlX = 0;
var orbitControlY = 0;
var orbitControlZ = 0;
var filter_tracing_active_cec = false;
function btnContactTracingIconCec(e) {
    if ($(e).hasClass("active")) {
        $(e).removeClass("active");
        $("#devicebox-button-contact-tracing-cec img").attr("src", base_url + "Content/assets/images/contact-tracing-dim.png");
        $("#windowPopup").data("kendoWindow").close();
        if (filter_tracing_active_cec) {
            filter_tracing_active_cec = false;
            cec_partnership_setBefore();
            $("#box-playback-tracing").html("");
        }

        
    } else {
        $(e).hasClass("active");
        $("#devicebox-button-contact-tracing-cec img").attr("src", base_url + "Content/assets/images/contact-tracing-active.png");
        openWindow('windowContactTracingCec', []);
    }
}
function btnContactTracingIconCecCCTV(e) {
    if ($("#devicebox-cec_cctv").is(":visible") == true) {
        $("#devicebox-cec_cctv").hide();
    } else {
        $("#devicebox-cec_cctv").show();
    }
}
function clearPlayback(){
    clearTimeout(playTime);
    currentTimePlay = undefined;
    currentSpeed = playBackSpeed[0];
    clearTimeout(playZoneTime);
    currentZoneTimePlay = undefined;
    currentZoneSpeed = playBackSpeed[0];
}
function contactTracing(uuid, personName){
    clearPlayback();
	$(".button-icon-contact-tracing").addClass("active");
	$("#devicebox-button-contact-tracing img").attr("src",base_url+"Content/assets/images/contact-tracing-active.png");
	$("#box-alert").html("");
	$("#box-human-presence").html("");
	$("#windowPopup").data("kendoWindow").close();
	filter_tracing_active = true;
	clear_all_avatar();
	set_all_default_zone();
	
	for(var i = 0; i<dummy_tracing_zone.length;i++){
		setColorByZoneTopOnly(dummy_tracing_zone[i], color_global.yellow, "");
    }
    var val = $("#contact-tracing-userId").val();
    if((uuid == "" || uuid == undefined) && val != undefined && val != "")
        uuid = val;
    if(uuid == "" || uuid == undefined)
        uuid = "H12345";
    if(personName == "" || personName == undefined)
        personName = "Person " + uuid.substr(uuid.length -1,1);
	//start load new content realtime
	var _itemTemplate = kendo.template($("#tmpl-WidgetRealtimeContactTracing").html(), { useWithBlock: false });
	let data_realtime = {
		TotalVisited: dummy_tracing_zone.length,
        TotalContacted: 50,
        uuid: uuid,
        personName: personName,
		data: dummy_tracing_timeline
	};
	var result = _itemTemplate(data_realtime); //Pass the data to the compiled template
	$("#wrapper-widget-realtime .widget-content").html(result);
    $("#wrapper-widget-realtime .widget-footer").addClass("hide");
    var today = kendo.toString(new Date(),"dd-MM-yyyy");
    $(".widget-realtime-content .title-realtime").html("Contact Tracing - " + today);
	$(".realtime-ContactTracing").parent().css("max-height","525px");
	$(".realtime-ContactTracing").closest(".widget-realtime-content").css("width","420px");
	
	if($("#dateContactTracing").data("kendoDatePicker") != undefined){
		var dateContactTracing = $("#dateContactTracing").data("kendoDatePicker").value();
		var dateString = kendo.toString(dateContactTracing, "dd-MM-yyyy HH:mm:ss");
		$("#headingRealtime span.title-realtime").html("Contact Tracing - "+dateString);
	}
	//end load new content realtime
	
	//show playback
	var _itemPlaybackTracing = kendo.template($("#tmpl-WidgetFloatingPlaybackContactTracing").html(), { useWithBlock: false });
	var resultPlaybackTracing = _itemPlaybackTracing({});
    $("#box-playback-tracing").html(resultPlaybackTracing);
    
    var dataLength = dummy_tracing_timeline.length;
    var date1 = new Date(dummy_tracing_timeline[0].Time);
    var date2 = new Date(dummy_tracing_timeline[dataLength-1].Time);
    var diff = (date2.getTime() - date1.getTime())/1000;

    $("#contact-tracing-playback-zone").kendoSlider({
        min: 1,
        max: diff,
        tooltip: {
            template: kendo.template("#= formatSliderTooltip(value) #")
        },
        change: playbackZoneOnChange,
        slide: playbackZoneOnSlide
    });
}
function contactTracingCec(uuid, personName) {
	removeBox("alert");
    cameraCecX = camera.position.x;
    cameraCecY = camera.position.y;
    cameraCecZ = camera.position.z;
    orbitControlX = orbitControls.target.x;
    orbitControlY = orbitControls.target.y;
    orbitControlZ = orbitControls.target.z;
    clearPlayback();
    $(".button-icon-contact-tracing").addClass("active");
    $("#devicebox-button-contact-tracing img").attr("src", base_url + "Content/assets/images/contact-tracing-active.png");
    $("#box-alert").html("");
    $("#box-sensor").html("");
    $("#box-human-presence").html("");
    $("#windowPopup").data("kendoWindow").close();
    filter_tracing_active_cec = true;

    human_building_cec();

    var val = $("#contact-tracing-userId-cec").val();
    if ((uuid == "" || uuid == undefined) && val != undefined && val != "")
        uuid = val;
    if (uuid == "" || uuid == undefined)
        uuid = "H12345";
    if (personName == "" || personName == undefined)
        //personName = "Person " + uuid.substr(uuid.length - 1, 1);
        if (personName == "" || personName == undefined) {
            personName = "Person " + uuid.substr(uuid.length - 1, 1);
        } else {
            personName = "Wujuan " + uuid.substr(uuid.length - 1, 1);
        }

    //start load new content realtime
    var _itemTemplate = kendo.template($("#tmpl-WidgetRealtimeContactTracing").html(), { useWithBlock: false });
    let data_realtime = {
        TotalVisited: dummy_tracing_zone.length,
        TotalContacted: 50,
        uuid: uuid,
        personName: personName,
        data: dummy_tracing_timeline
    };
    var result = _itemTemplate(data_realtime); //Pass the data to the compiled template
    $("#wrapper-widget-realtime .widget-content").html(result);
    $("#wrapper-widget-realtime .widget-footer").addClass("hide");
    var today = kendo.toString(new Date(), "dd-MM-yyyy");
    $(".widget-realtime-content .title-realtime").html("Contact Tracing - " + today);
    $(".realtime-ContactTracing").parent().css("max-height", "525px");
    $(".realtime-ContactTracing").closest(".widget-realtime-content").css("width", "420px");

    if ($("#dateContactTracing").data("kendoDatePicker") != undefined) {
        var dateContactTracing = $("#dateContactTracing").data("kendoDatePicker").value();
        var dateString = kendo.toString(dateContactTracing, "dd-MM-yyyy HH:mm:ss");
        $("#headingRealtime span.title-realtime").html("Contact Tracing - " + dateString);
    }
    //end load new content realtime

    //show playback
    var _itemPlaybackTracing = kendo.template($("#tmpl-WidgetFloatingPlaybackContactTracingCEC").html(), { useWithBlock: false });
    var resultPlaybackTracing = _itemPlaybackTracing({});
    $("#box-playback-tracing").html(resultPlaybackTracing);

    var dataLength = dummy_tracing_timeline.length;
    var date1 = new Date(dummy_tracing_timeline[0].Time);
    var date2 = new Date(dummy_tracing_timeline[dataLength - 1].Time);
    var diff = (date2.getTime() - date1.getTime()) / 1000;

    $("#contact-tracing-playback-zone").kendoSlider({
        min: 1,
        max: diff,
        tooltip: {
            template: kendo.template("#= formatSliderTooltip(value) #")
        },
        change: playbackZoneOnChange,
        slide: playbackZoneOnSlide
    });
}
function formatSliderTooltip(value){
    var date1 = new Date(dummy_tracing_timeline[0].Time).getTime() + (value * delayZoneTime);
    return kendo.toString(new Date(date1), "HH:mm");
}
function contactTracingDetail(i){
	clear_all_avatar();
	set_all_default_zone();
	setColorByZoneTopOnly(dummy_tracing_zone[i], color_global.yellow, "");	
	var avatar = gltf.scene.children.find(f=>f.name == dummy_tracing_avatar[i]);
	if(avatar != undefined){
		avatar.visible = true;
		avatar.material.visible = true;
		avatar.material.color.setHex(color_global.red);
	}
	$(".ContactTracingHeader").hide();
	$(".ContactTracingDetail").show();
	$(".ContactTracingDetail .time-contact-tracing").html(kendo.toString(new Date(dummy_tracing_timeline[i].Time),"HH:mm"));
	$(".ContactTracingDetail .zonename-contact-tracing-title").html(dummy_tracing_timeline[i].ZoneName);
    var a = i+1;
	var contactData = dummy_tracing_timeline[i].contactData;

    $(".ContactTracingDetail .zonename-contact-tracing-index").html(a);
    $(".ContactTracingDetail .persons-contact-tracing-title span").html(contactData.length);
	var html = "";
	for(var c=0; c < contactData.length; c++){
        var classHighSkin = (contactData[c].IsHighSkin) ? "bg-contact-tracing-danger": "bg-contact-tracing-normal";
        var icon_user = (contactData[c].IsHighSkin) ? base_url + 'Content/assets/images/icon-user-danger.png' : base_url + 'Content/assets/images/icon-user.png';
		html += '<li>'+
					"<a href=\"javascript:void(0);\" onclick=\"contactTracingIndividual('"+contactData[c].user+"')\">"+
						'<img src="'+icon_user+'" width="30" class="icon-contact-tracing"/>'+
						'<span class="personid-contact-tracing '+classHighSkin+'">'+ contactData[c].user +'</span>'+
						'<span class="personname-contact-tracing '+classHighSkin+'">'+ contactData[c].name +'</span>'+
					'</a>'+
				'</li>';
	}
	$(".persons-contact-tracing").html(html);
	//
}
function contactTracingIndividual(user){
    console.log(user);
    openWindow('windowContactTracing',[]);
    $("#contact-tracing-userId").val(user);
    $("#contact-tracing-userId-cec").val(user);
}
function playbackOnChange(ev){
    var evOnTime = new Date(dummy_tracing_timeline[0].Time).getTime() + (ev.value * delayTime);
    var check = dummy_tracing_timeline.filter(f=> new Date(f.Time) <= new Date(evOnTime));
    
    var windows_title = "Contact Tracing - "+kendo.toString(new Date(evOnTime),"dd-MM-yyyy HH:mm:ss");
    $("#windowPopup").data("kendoWindow").title(windows_title);

    if(check.length > 0){
        var curTime = new Date(check[0].Time);
        var idx = 0;
        
        check.map(function(temp,i){
            if(new Date(temp.Time) > curTime){
                curTime = new Date(temp.Time);
                idx = i;
            }
        });
        var top = dummy_position_avatar[idx].top;
        var left = dummy_position_avatar[idx].left;
        $(".person-icon-playback").attr("style","left: "+left+"; top: "+top);
        $(".playback-rightside .zonename").html("Zone Name: " + dummy_tracing_timeline[idx].ZoneName);
        contactTracingDetail(idx);
    }	
}
function playbackOnSlide(ev){
    var date1 = new Date(dummy_tracing_timeline[0].Time);
    var newCurrent = date1.getTime() + (ev.value * delayTime);
    currentTimePlay = new Date(newCurrent);
    
    var lastValue = (currentTimePlay.getTime() - date1.getTime())/1000;
    $("#popup-contact-tracing-playback-action").data("kendoSlider").value(lastValue);
    $("#popup-contact-tracing-playback-action").data("kendoSlider").trigger("change", { value: lastValue });
}

var playBackSpeed = [
    1000,//1s
    60000,//1min
    120000,//2min
    240000,//4min
    480000,//8min
];
/**
 * VARIABLE PLAYBACK POPUP
 */
var playTime;
var currentSpeed = playBackSpeed[0];//1s
var delayTime = playBackSpeed[0];//1s
var currentTimePlay;

function playbackButton(){
    var playCmd = $(".popup-contact-tracing-playback-button a i#playButton").hasClass("fa-play");
    contactTracingDetail(0);
	if(playCmd){
        var dataLength = dummy_tracing_timeline.length;
        var date1 = new Date(dummy_tracing_timeline[0].Time);
        var date2 = new Date(dummy_tracing_timeline[dataLength-1].Time);
        currentTimePlay = date1;
        playTime = setInterval(function(){
            const currentToTime = new Date(currentTimePlay);
            if(currentToTime < date2){
                currentTimePlay = new Date(currentTimePlay).setMilliseconds(currentSpeed);
                var windows_title = "Contact Tracing - "+kendo.toString(new Date(currentTimePlay),"dd-MM-yyyy HH:mm:ss");
                $("#windowPopup").data("kendoWindow").title(windows_title);
                
                var currentTime = new Date(currentTimePlay);
                var lastValue = (currentTime.getTime() - date1.getTime())/1000;
                $("#popup-contact-tracing-playback-action").data("kendoSlider").value(lastValue);
                $("#popup-contact-tracing-playback-action").data("kendoSlider").trigger("change", { value: lastValue });
            }else{
                playbackButton();
                //clearInterval(playTime);
                //currentTimePlay = undefined;
            }
            
        },delayTime);
		// for(var i = 0; i < dummy_position_avatar.length;i++){
		// 	playbackSet(i);
		// }
		$(".popup-contact-tracing-playback-button a i#playButton").removeClass("fa-play");
		$(".popup-contact-tracing-playback-button a i#playButton").addClass("fa-stop");
	}else{
		clearTimeout(playTime);
		var windows_title = "Contact Tracing - "+kendo.toString(new Date(dummy_tracing_timeline[0].Time),"dd-MM-yyyy HH:mm:ss");
		$("#windowPopup").data("kendoWindow").title(windows_title);
		$(".popup-contact-tracing-playback-button a i#playButton").removeClass("fa-stop");
		$(".popup-contact-tracing-playback-button a i#playButton").addClass("fa-play");
		$("#popup-contact-tracing-playback-action").data("kendoSlider").value(1);
		$("#popup-contact-tracing-playback-action").data("kendoSlider").trigger("change", { value: 1 });
	}
}
function playbackForwardButton(){
    var playCmd = $(".popup-contact-tracing-playback-button a i#playButton").hasClass("fa-play");
	if(!playCmd && currentSpeed >= playBackSpeed[0] && currentSpeed <= playBackSpeed[4]){
        var currentIndexSpeed = playBackSpeed.findIndex(f=> f == currentSpeed);
        if(currentIndexSpeed < 4){
            currentSpeed = playBackSpeed[currentIndexSpeed + 1];
            $("#current-speed").html("SPEED: x" + (currentIndexSpeed + 1));
            $(".speed-desc p").html("Playback Speed x" + (currentIndexSpeed + 1) + " (1sec = "+ (currentSpeed/60000) +"min)");
        }else{
            currentSpeed = playBackSpeed[4];
            $("#current-speed").html("SPEED: x4");
            $(".speed-desc p").html("Playback Speed x4 (1sec = 8min)");
        }
    }
}
function playbackBackwardButton(){
    var playCmd = $(".popup-contact-tracing-playback-button a i#playButton").hasClass("fa-play");
	if(!playCmd && currentSpeed >= playBackSpeed[0] && currentSpeed <= playBackSpeed[4]){
        var currentIndexSpeed = playBackSpeed.findIndex(f=> f == currentSpeed);
        if(currentIndexSpeed > 0){
            currentSpeed = playBackSpeed[currentIndexSpeed - 1];
            $("#current-speed").html("SPEED: x" + (currentIndexSpeed - 1));

            var min = (currentSpeed/60000) < 1 ? 1 + "sec" : (currentSpeed/60000) + "min";
            
            $(".speed-desc p").html("Playback Speed x" + (currentIndexSpeed - 1) + " (1sec = "+ min +")");
        }else{
            currentSpeed = playBackSpeed[0];
            $("#current-speed").html("SPEED: x0");
            $(".speed-desc p").html("Playback Speed x0 (1sec = 1sec)");
        }
    }
}
/**
 * VARIABLE PLAYBACK ZONE
 */
var playZoneTime;
var currentZoneSpeed = playBackSpeed[0];//1s
var delayZoneTime = playBackSpeed[0];//1s
var currentZoneTimePlay;
function playbackZoneOnChange(ev){
    var evOnTime = new Date(dummy_tracing_timeline[0].Time).getTime() + (ev.value * delayZoneTime);
    var check = dummy_tracing_timeline.filter(f=> new Date(f.Time) <= new Date(evOnTime));
    
    var title_realtime = "Contact Tracing - "+kendo.toString(new Date(evOnTime),"dd-MM-yyyy | HH:mm:ss");
    $(".widget-realtime-content .title-realtime").html(title_realtime);

    if(check.length > 0){
        var curTime = new Date(check[0].Time);
        var idx = 0;
        
        check.map(function(temp,i){
            if(new Date(temp.Time) > curTime){
                curTime = new Date(temp.Time);
                idx = i;
            }
        });
        //zone change color
        contactTracingDetail(idx);
    }	
}
function playbackZoneOnSlide(ev){
    var date1 = new Date(dummy_tracing_timeline[0].Time);
    var newCurrent = date1.getTime() + (ev.value * delayZoneTime);
    currentZoneTimePlay = new Date(newCurrent);
    
    var lastValue = (currentZoneTimePlay.getTime() - date1.getTime())/1000;
    $("#contact-tracing-playback-zone").data("kendoSlider").value(lastValue);
    $("#contact-tracing-playback-zone").data("kendoSlider").trigger("change", { value: lastValue });
}
function playbackZoneButton(){
    var playCmd = $(".contact-tracing-playback-zone-action a i#playZoneButton").hasClass("fa-play");
    contactTracingDetail(0);
	if(playCmd){
        var dataLength = dummy_tracing_timeline.length;
        var date1 = new Date(dummy_tracing_timeline[0].Time);
        var date2 = new Date(dummy_tracing_timeline[dataLength-1].Time);
        currentZoneTimePlay = date1;
        playZoneTime = setInterval(function(){
            const currentToTime = new Date(currentZoneTimePlay);
            if(currentToTime < date2){
                currentZoneTimePlay = new Date(currentZoneTimePlay).setMilliseconds(currentZoneSpeed);
                var title_realtime = "Contact Tracing - "+kendo.toString(new Date(currentZoneTimePlay),"dd-MM-yyyy | HH:mm:ss");
                $(".widget-realtime-content .title-realtime").html(title_realtime);

                var currentTime = new Date(currentZoneTimePlay);
                var lastValue = (currentTime.getTime() - date1.getTime())/1000;
                $("#contact-tracing-playback-zone").data("kendoSlider").value(lastValue);
                $("#contact-tracing-playback-zone").data("kendoSlider").trigger("change", { value: lastValue });
            }else{
                playbackZoneButton();
                //clearInterval(playZoneTime);
                //currentZoneTimePlay = undefined;
            }
            
        },delayZoneTime);
		// for(var i = 0; i < dummy_position_avatar.length;i++){
		// 	playbackSet(i);
		// }
		$(".contact-tracing-playback-zone-action a i#playZoneButton").removeClass("fa-play");
		$(".contact-tracing-playback-zone-action a i#playZoneButton").addClass("fa-stop");
	}else{
        clearTimeout(playZoneTime);
        
		$(".contact-tracing-playback-zone-action a i#playZoneButton").removeClass("fa-stop");
		$(".contact-tracing-playback-zone-action a i#playZoneButton").addClass("fa-play");
		$("#contact-tracing-playback-zone").data("kendoSlider").value(1);
		$("#contact-tracing-playback-zone").data("kendoSlider").trigger("change", { value: 1 });
	}
}
function playbackZoneForwardButton(){
    var playCmd = $(".contact-tracing-playback-zone-action a i#playZoneButton").hasClass("fa-play");
	if(!playCmd && currentZoneSpeed >= playBackSpeed[0] && currentZoneSpeed <= playBackSpeed[4]){
        var currentIndexSpeed = playBackSpeed.findIndex(f=> f == currentZoneSpeed);
        if(currentIndexSpeed < 4){
            currentZoneSpeed = playBackSpeed[currentIndexSpeed + 1];
            $("#current-speed-zone").html("Speed: x" + (currentIndexSpeed + 1));
        }else{
            currentZoneSpeed = playBackSpeed[4];
            $("#current-speed-zone").html("Speed: x4");
        }
    }
}
function playbackZoneBackwardButton(){
    var playCmd = $(".contact-tracing-playback-zone-action a i#playZoneButton").hasClass("fa-play");
	if(!playCmd && currentZoneSpeed >= playBackSpeed[0] && currentZoneSpeed <= playBackSpeed[4]){
        var currentIndexSpeed = playBackSpeed.findIndex(f=> f == currentZoneSpeed);
        if(currentIndexSpeed > 0){
            currentZoneSpeed = playBackSpeed[currentIndexSpeed - 1];
            $("#current-speed-zone").html("Speed: x" + (currentIndexSpeed - 1));
        }else{
            currentZoneSpeed = playBackSpeed[0];
            $("#current-speed-zone").html("Speed: x0");
        }
    }
}
